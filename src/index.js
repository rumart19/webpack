// JS
import './js/'

// SCSS
import './assets/scss/main.scss'

// CSS (example)
// import './assets/css/main.css'

// Vue.js
window.Vue = require('vue')

// Vue components (for use in html)
// Vue.component('map-component', require('./components/map.vue').default)
// Vue.component('form-component', require('./components/form.vue').default)
// Vue.component('keyboard-component', require('./components/SimpleKeyboard.vue').default)

// Vue init
// const app = new Vue({
//   el: '#app',
//   data: {
//     sendForm: {
//       fio: '',
//       phone: '',
//       title: '',
//     },
//     menuOpen: false,
//   }
// })
