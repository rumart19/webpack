## Установка:

``` bash

# Установка:
npm install

# Команда для разработки на http://localhost:8081/
npm run dev

# Команда для сборки
npm run build
```

## Структура проекта:

* `src/pug/layout` - `pug` шаблоны страниц
* `src/pug/includes` - `pug` области для вставки
* `src/pug/utils` - `pug` миксины
* `src/pug/pages` - `pug` страницы
* `src/assets/scss` - `scss` стили
* `src/assets/css` - `css` стили
* `src/assets/img` - Картинки
* `src/js` - `js` файлы
* `src/index.js` - Основной `js` файл
* `src/components` - Папка для компонентов `.vue` components
* `static/` - Папка для статики